// +build ignore

package main

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
)

var (
	oFlag string

	octiconVersion = "9.3.1"
)

const (
	tgzName = "octicons.tgz"
	dirName = "octicons"
)

func init() {
	flag.StringVar(&oFlag, "o", "", "write output to `file` (default standard output)")
}

func main() {
	flag.Parse()

	err := run()
	if err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	// Get octicons
	resp, err := http.Get(fmt.Sprintf("https://registry.npmjs.org/@primer/octicons/-/octicons-%s.tgz", octiconVersion))
	if err != nil {
		return fmt.Errorf("get octicons: %v", err)
	}
	defer resp.Body.Close()

	tgz, err := os.Create(tgzName)
	if err != nil {
		return fmt.Errorf("create %s: %v", tgzName, err)
	}
	defer os.Remove(tgzName)

	if _, err = io.Copy(tgz, resp.Body); err != nil {
		return fmt.Errorf("copy octicons: %v", err)
	}

	if err := tgz.Close(); err != nil {
		return fmt.Errorf("close %s: %v", tgzName, err)
	}

	tgz, err = os.Open(tgzName)
	if err != nil {
		return fmt.Errorf("open %s: %v", tgzName, err)
	}

	// Unzip
	if err := os.MkdirAll(dirName, os.ModePerm); err != nil {
		return fmt.Errorf("mkdirall %s: %v", dirName, err)
	}
	defer os.RemoveAll(dirName)

	if err := unzip(dirName, tgz); err != nil {
		return fmt.Errorf("unzip octicons: %v", err)
	}

	// Generate
	octicons := make(map[string]string, 0)
	if err := filepath.Walk(dirName+"/package/build/svg", func(path string, info os.FileInfo, walkErr error) error {
		if walkErr != nil {
			return fmt.Errorf("walk error: %v", walkErr)
		}

		if info.IsDir() {
			return nil
		}

		name := strings.TrimSuffix(filepath.Base(path), ".svg")

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return fmt.Errorf("read file '%s': %v", path, err)
		}

		octicons[name] = string(data)

		return nil
	}); err != nil {
		return fmt.Errorf("walk svgs: %v", err)
	}

	var names []string
	for name := range octicons {
		names = append(names, name)
	}
	sort.Strings(names)

	var buf bytes.Buffer
	fmt.Fprint(&buf, `package octicon

import (
	"fmt"
	"html/template"
)

// Icons is a list of all Feather icons
var Icons = []string{"`)

	fmt.Fprintf(&buf, strings.Join(names, `", "`))

	fmt.Fprintf(&buf, `"}

// Octicon represents an SVG node
type Octicon struct {
	xml    string
	width  int
	height int
	style  string
	id     string
	class  string
}

// XML returns the SVG node as an XML string
func (o *Octicon) XML() string {
	return fmt.Sprintf(o.xml, o.width, o.height, o.style, o.id, o.class)
}

// HTML returns the SVG node as an HTML template, safe for use in Go templates
func (o *Octicon) HTML() template.HTML {
	return template.HTML(o.XML())
}

// Size sets the size of an Octicon
// Short for calling Width and Height with the same int
func (o *Octicon) Size(size int) {
	o.Width(size)
	o.Height(size)
}

// Width sets the width of an Octicon
func (o *Octicon) Width(width int) {
	o.width = width
}

// Height sets the height of an Octicon
func (o *Octicon) Height(height int) {
	o.height = height
}

// Style sets the style of an Octicon
func (o *Octicon) Style(style string) {
	o.style = style
}

// Id sets the id of an Octicon
func (o *Octicon) Id(id string) {
	o.id = id
}

// Class sets the class of an Octicon
func (o *Octicon) Class(class string) {
	o.class = class
}

// Icon returns the named Octicon SVG node.
// It returns nil if name is not a valid Octicon symbol name.
func Icon(name string) *Octicon {
	switch name {
`)
	for _, name := range names {
		fmt.Fprintf(&buf, "	case %q:\n		return %v()\n", name, kebab(name))
	}
	fmt.Fprint(&buf, `	default:
		return nil
	}
}
`)

	// Write all individual Octicon functions.
	for _, name := range names {
		generateAndWriteOcticon(&buf, octicons, name)
	}

	var w io.Writer
	switch oFlag {
	case "":
		w = os.Stdout
	default:
		f, err := os.Create(oFlag)
		if err != nil {
			return err
		}
		defer f.Close()
		w = f
	}

	_, err = w.Write(buf.Bytes())
	return err
}

var octiconTemplate = template.Must(template.New("octicon").Parse(`return &Octicon{
	xml: ` + "`" + `{{.xml}}` + "`" + `,
	width: 16,
	height: 16,
	style: "display: inline-block; vertical-align: text-top; fill: currentColor;",
}
`))

func generateAndWriteOcticon(w io.Writer, ionicons map[string]string, name string) {
	fmt.Fprintln(w)
	fmt.Fprintf(w, "// %s returns the %q Octicon.\n", kebab(name), name)
	fmt.Fprintf(w, "func %s() *Octicon {\n", kebab(name))

	data := ionicons[name]
	page, err := html.Parse(bytes.NewBufferString(data))
	if err != nil {
		fmt.Println(err)
		return
	}
	node := page.FirstChild.LastChild.FirstChild
	node.Attr[1].Val = "%d"
	node.Attr[2].Val = "%d"
	node.Attr = append(node.Attr,
		html.Attribute{Key: "style", Val: "%s"},
		html.Attribute{Key: "id", Val: "%s"},
		html.Attribute{Key: "class", Val: "%s"},
	)
	node.InsertBefore(&html.Node{
		FirstChild: &html.Node{
			Type:      html.TextNode,
			Data:      name,
			Namespace: "svg",
		},
		Type:      html.ElementNode,
		DataAtom:  atom.Title,
		Data:      "title",
		Namespace: "svg",
	}, node.FirstChild)

	xml := &strings.Builder{}
	if err := html.Render(xml, node); err != nil {
		fmt.Println(err)
		return
	}

	octiconTemplate.Execute(w, map[string]interface{}{
		"xml": xml.String(),
	})
	fmt.Fprintln(w, "}")
}

func kebab(input string) string {
	parts := make([]string, strings.Count(input, "-")+1)
	for idx, part := range strings.Split(input, "-") {
		parts[idx] = strings.Title(part)
	}
	return strings.Join(parts, "")
}

func unzip(dst string, r io.Reader) error {

	gzr, err := gzip.NewReader(r)
	if err != nil {
		return fmt.Errorf("gzip reader: %v", err)
	}
	defer gzr.Close()

	tr := tar.NewReader(gzr)

	for {
		header, err := tr.Next()

		switch {

		// if no more files are found return
		case err == io.EOF:
			return nil

		// return any other error
		case err != nil:
			return err

		// if the header is nil, just skip it (not sure how this happens)
		case header == nil:
			continue
		}

		// the target location where the dir/file should be created
		target := filepath.Join(dst, header.Name)

		if _, err := os.Stat(target); err != nil {
			if err := os.MkdirAll(filepath.Dir(target), 0755); err != nil {
				return err
			}
		}

		f, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
		if err != nil {
			return err
		}

		// copy over contents
		if _, err := io.Copy(f, tr); err != nil {
			return err
		}

		// manually close here after each file operation; deferring would cause each file close
		// to wait until all operations have completed.
		f.Close()
	}
}
